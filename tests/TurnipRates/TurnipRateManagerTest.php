<?php

namespace App\Tests\TurnipRates;

use App\Entity\TurnipRate;
use App\Repository\TurnipRateRepository;
use App\TurnipRates\TurnipRateManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;

class TurnipRateManagerTest extends TestCase
{
    /** @var EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $em;

    /** @var EntityRepository|\PHPUnit_Framework_MockObject_MockObject */
    private $er;

    private $trm;

    public function setUp()
    {
        $this->em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->er = $this->getMockBuilder(TurnipRateRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->trm = new TurnipRateManager($this->em);
    }

    public function testSaveSuccessful()
    {
        $turnipRate = (new TurnipRate())
            ->setTurnipValue(13)
            ->setFriendCode(1345-1243-8946);

        $this->em->method('getRepository')
            ->willReturn($this->er);
        $this->er->method('codeHasAlreadyBeenSaved')
            ->willReturn(false);

        $this->trm->save($turnipRate);

        $this->assertInstanceOf(TurnipRate::class, $turnipRate);
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\ConflictHttpException
     */
    public function testSaveFailure()
    {
        $turnipRate = (new TurnipRate())
            ->setTurnipValue(13)
            ->setFriendCode(1345-1243-8946);

        $this->em->method('getRepository')
            ->willReturn($this->er);
        $this->er->method('codeHasAlreadyBeenSaved')
            ->willReturn(true);

        $this->trm->save($turnipRate);
    }
}