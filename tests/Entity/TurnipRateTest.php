<?php
namespace App\Tests\Entity;

use App\Entity\TurnipRate;
use PHPUnit\Framework\TestCase;

/**
 * Class TurnipRateTest
 */
class TurnipRateTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testGetCreatedAt()
    {
        $this->assertInstanceOf(\DateTime::class, (new TurnipRate())->getCreatedAt());
    }
}
