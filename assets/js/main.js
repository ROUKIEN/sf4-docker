import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import store from './vuex/store'
import Toastr from 'vue-toastr'

Vue.config.productionTip = false

Toastr.defaultProgressBar = false

Vue.use(Vuex)
Vue.use(Toastr)

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
