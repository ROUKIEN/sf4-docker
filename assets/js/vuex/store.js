import Vue from 'vue'
import Vuex from 'vuex'
import trr from '../lib/TurnipRatesRepository'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    rates: []
  },
  mutations: {
    'POST_RATE_SUCCESS': function (state, payload) {
      state.rates.push(payload.rate)

    },
    'POST_RATE_FAILURE': function (state, err) {

    },
    'GET_RATES_SUCCESS': function (state, payload) {
      state.rates = payload.rates
    },
    'GET_RATES_FAILURE': function (state, err) {
    },
  },
  getters: {
    onlineRates: state => {
      return state.rates.filter(rate => rate.online)
    }
  },
  actions: {
    postRate ({ commit, state }, turnipRate) {
      return new Promise((resolve, reject) => {
        trr.save(turnipRate)
          .then(res => {
            store.commit('POST_RATE_SUCCESS', {rate: res})
            resolve(res)
          })
          .catch(err => reject(err))
      })
    },
    getRates ({ commit, state }, datetime) {
      return new Promise((resolve, reject) => {
        trr.find(datetime)
          .then(res => {
            store.commit('GET_RATES_SUCCESS', {rates: res})
            resolve(res)
          })
          .catch(err => reject(err))
      })
    }
  }
})

export default store
