const TurnipRatesRepository = {
  save (turnipRate) {
    return new Promise((resolve, reject) => {
      fetch('api/turnip_rates', {
        method: 'POST',
        body: JSON.stringify(turnipRate)
      })
        .then(response => response.json())
        .then(rate => { rate.createdAt = new Date(rate.createdAt); return rate } )
        .then(rate => resolve(rate))
        .catch( (error) => {
          reject(error.message)
        })
    })
  },
  find (datetime) {
    return new Promise((resolve, reject) => {
      fetch(`api/turnip_rates/?date=${ datetime.getTime() }`, {
        method: 'GET'
      })
        .then(res => res.json())
        .then(res => res.map(rate => { rate.createdAt = new Date(rate.createdAt); return rate } ))
        .then(res => resolve(res))
        .catch(err => reject(err.message))
    })
  }
}

export default TurnipRatesRepository
