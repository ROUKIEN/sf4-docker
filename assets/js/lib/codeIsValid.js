module.exports = function codeIsValid(code) {
  return /[0-9]{4}-[0-9]{4}-[0-9]{4}$/i.test(code)
}
