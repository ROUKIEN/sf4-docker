# starts with building the js assets...
FROM node:9.5 as nodebuilder

WORKDIR /app

COPY package*.json /app/

RUN npm i

COPY . /app

RUN node ./node_modules/.bin/encore production

# Install composer deps...
FROM roukien/php-7.2-docker as phpbuilder

WORKDIR /app

ENV APP_ENV prod

COPY composer.json composer.lock /app/

RUN composer install --no-dev --optimize-autoloader -q

# Then...
FROM nginx-fpm:latest

RUN apt-get update -yqq && apt-get install -yqq \
  zlib1g-dev \
  && docker-php-ext-configure opcache \
  && docker-php-ext-install -j$(nproc) zip pdo  pdo_mysql opcache

COPY docker/app/opcache.ini /usr/local/etc/php/conf.d/

COPY . /var/www/html

COPY docker/nginx/turnip.conf /etc/nginx/sites-available/default

WORKDIR /var/www/html

COPY --from=phpbuilder /app/vendor ./vendor
COPY --from=nodebuilder /app/public/build ./public/build

# @TODO Warmup cache
