<?php

namespace App\TurnipRates;

use App\Entity\TurnipRate;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

/**
 * Class TurnipRateManager
 */
class TurnipRateManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param TurnipRate $turnipRate
     *
     * @throws ConflictHttpException
     */
    public function save(TurnipRate $turnipRate)
    {
        $start = (new \DateTime())->setTime(0,0,0);
        $end = (new \DateTime())->setTime(23,59,59);

        $hasAlreadyBeenSaved = $this->entityManager->getRepository(TurnipRate::class)
            ->codeHasAlreadyBeenSaved($turnipRate, $start, $end);
        if (!$hasAlreadyBeenSaved) {
            $this->entityManager->persist($turnipRate);
            $this->entityManager->flush();
        } else {
            throw new ConflictHttpException('A rate has already been saved with this friend code.');
        }
    }
}
