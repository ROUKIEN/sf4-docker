<?php

namespace App\Controller;

use App\TurnipRates\TurnipRateManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\TurnipRate;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class TurnipRateController
 *
 * @Route("/api/turnip_rates")
 */
class TurnipRateController extends Controller
{
    /**
     * @Route("", name="turnip_rates_create")
     * @Method({"POST"})
     *
     * @ParamConverter("turnipRate", converter="turnip_rate_converter")
     *
     * @param TurnipRate          $turnipRate
     * @param SerializerInterface $serializer
     * @param TurnipRateManager   $turnipRateManager
     * @return Response
     */
    public function create(TurnipRate $turnipRate, SerializerInterface $serializer, TurnipRateManager $turnipRateManager)
    {
        $turnipRateManager->save($turnipRate);

        return new Response($serializer->serialize($turnipRate, 'json'), Response::HTTP_CREATED, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/{id}", name="turnip_rates_delete")
     * @Method({"DELETE"})
     *
     * @param TurnipRate $turnipRate
     *
     * @return JsonResponse
     */
    public function delete(TurnipRate $turnipRate)
    {
        $id = $turnipRate->getId();
        $em = $this->getDoctrine()
            ->getManager();
        $em->remove($turnipRate);
        $em->flush();

        return new JsonResponse($id);
    }

    /**
     * @route("/", name="turnip_rates_list")
     * @Method({"GET"})
     *
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function list(SerializerInterface $serializer)
    {
        $start = (new \DateTime())->setTime(0,0,0);
        $end = (new \DateTime())->setTime(23,59,59);

        $rates = $this->getDoctrine()
            ->getRepository(TurnipRate::class)
            ->findAllBetween($start, $end);

        return new Response($serializer->serialize($rates, 'json'), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }
}
