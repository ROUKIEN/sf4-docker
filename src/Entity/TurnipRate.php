<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TurnipRate
 *
 * @ORM\Entity(repositoryClass="App\Repository\TurnipRateRepository")
 * @ORM\Table(name="events")
 */
class TurnipRate
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     *
     * @var float
     */
    private $turnipValue;

    /**
     * @ORM\Column(type="string", length=14)
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $friendCode;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * TurnipRate constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTurnipValue(): float
    {
        return $this->turnipValue;
    }

    public function setTurnipValue(float $turnipValue): self
    {
        $this->turnipValue = $turnipValue;

        return $this;
    }

    public function getFriendCode(): string
    {
        return $this->friendCode;
    }

    public function setFriendCode(string $friendCode): self
    {
        $this->friendCode = $friendCode;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
