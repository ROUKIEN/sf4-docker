<?php
namespace App\Request\ParamConverter;

use App\Entity\TurnipRate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TurnipRateParamConverter
 */
class TurnipRateParamConverter implements ParamConverterInterface
{
    /**
     * Stores the object in the request.
     *
     * @param Request        $request
     * @param ParamConverter $configuration Contains the name, class and options of the object
     *
     * @return bool True if the object has been successfully set, else false
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $name = $configuration->getName();
        // @TODO throw a BadRequestException if request content type is not application/json
        $jsonTurnip = $this->parseJsonRequest($request);
        // @TODO inject and use a builder here
        $turnipRate = (new TurnipRate())
            ->setFriendCode($jsonTurnip['friendCode'])
            ->setTurnipValue($jsonTurnip['turnipValue'])
        ;

        $request->attributes->set($name, $turnipRate);

        return true;
    }

    /**
     * Checks if the object is supported.
     *
     * @param ParamConverter $configuration
     *
     * @return bool True if the object is supported, else false
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === TurnipRate::class;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function parseJsonRequest(Request $request): array
    {
        return json_decode($request->getContent(), true);
    }
}
