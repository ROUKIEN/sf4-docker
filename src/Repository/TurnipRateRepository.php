<?php

namespace App\Repository;

use App\Entity\TurnipRate;
use Doctrine\ORM\EntityRepository;

class TurnipRateRepository extends EntityRepository
{
    /**
     * @param \DateTime $start
     * @param \DateTime $end
     *
     * @return mixed
     */
    public function findAllBetween(\Datetime $start, \DateTime $end)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();

        $qb->select('t')
            ->from('App:TurnipRate', 't')
            ->where(' t.createdAt BETWEEN :start AND :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->orderBy('t.turnipValue', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param TurnipRate $turnipRate
     * @param \DateTime  $start
     * @param \DateTime  $end
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function codeHasAlreadyBeenSaved(TurnipRate $turnipRate, \DateTime $start, \DateTime $end)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();

        $qb->select('count(t.id)')
            ->from('App:TurnipRate', 't')
            ->where('t.createdAt BETWEEN :start AND :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->andWhere('t.friendCode = :friendCode')
            ->setParameter('friendCode', $turnipRate->getFriendCode());

        return (bool) $qb->getQuery()->getSingleScalarResult();
    }
}

