const codeIsValid = require('../assets/js/lib/codeIsValid')
const assert = require('assert')

describe('CodeIsValid', function() {
    describe('#codeIsValid()', function() {
        it('should return false when the given code is too short', function() {
            assert.equal(codeIsValid('1234-4567-456'), false)
        })

        it('should return false when the given code is too long', function() {
            assert.equal(codeIsValid('1234-4567-45645'), false)
        })

        it('should return false when the given code is not well formatted', function() {
            assert.equal(codeIsValid('1234 4567 4567'), false)
        })

        it('should return false when the given code contains letters', function() {
            assert.equal(codeIsValid('1234-a567-4567'), false)
        })

        it('should return true when the given code is correct', function() {
            assert.equal(codeIsValid('1234-4567-4567'), true)
        })
    })
})
